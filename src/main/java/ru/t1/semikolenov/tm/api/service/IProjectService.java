package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}
