package ru.t1.semikolenov.tm.repository;

import ru.t1.semikolenov.tm.api.repository.ICommandRepository;
import ru.t1.semikolenov.tm.constant.ArgumentConst;
import ru.t1.semikolenov.tm.constant.TerminalConst;
import ru.t1.semikolenov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Show commands list."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show arguments list."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Display task by index."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Display task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id."
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index."
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Display project by index."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Display project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            COMMANDS, ARGUMENTS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }

}
